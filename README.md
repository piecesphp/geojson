# piecesphp/geojson

## Namespaces
- PiecesPHP\GeoJSON
- PiecesPHP\GeoJSON\Geometry

## Classes
- PiecesPHP\GeoJSON\GeoJson
- PiecesPHP\GeoJSON\Feature
- PiecesPHP\GeoJSON\Geometry\LineString
- PiecesPHP\GeoJSON\Geometry\MultiLineString
- PiecesPHP\GeoJSON\Geometry\MultiPolygon
- PiecesPHP\GeoJSON\Geometry\Point
- PiecesPHP\GeoJSON\Geometry\Polygon

## Interfaces
- PiecesPHP\GeoJSON\Geometry\GeometryInterface

## API
TODO
