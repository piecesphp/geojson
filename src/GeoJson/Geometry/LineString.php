<?php

/**
 * LineString.php
 */
namespace PiecesPHP\GeoJson\Geometry;

/**
 * LineString.
 *
 * Modelo de datos de geometría LineString
 *
 * @package     PiecesPHP\GeoJson\Geometry
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 * @see https://geojson.org/schema/LineString.json
 */
class LineString implements GeometryInterface
{

    const TYPE = 'LineString';

    /**
     * @var array
     */
    private $coordinates = [];

    /**
     * @var int
     */
    private $coordinateOrderMode = GeometryInterface::ORDER_LAT_LNG;

    /**
     * @param array $coordinates
     * @param int $coordinateOrderMode
     * @return static
     */
    public function __construct(array $coordinates = [], int $coordinateOrderMode = null)
    {

        if ($coordinateOrderMode !== null && ($coordinateOrderMode == GeometryInterface::ORDER_LAT_LNG || $coordinateOrderMode == GeometryInterface::ORDER_LNG_LAT)) {
            $this->coordinateOrderMode = $coordinateOrderMode;
        }

        $this->coordinates($coordinates);

    }

    /**
     * @param array $value
     * @return array|static
     */
    public function coordinates(array $value = null)
    {

        if (is_array($value) && count($value) > 0) {

            $coordinates = $value;

            $ok = false;

            if (count($coordinates) >= 2) {

                foreach ($coordinates as $index => $coordinate) {

                    if (is_array($coordinate)) {

                        if (count($coordinate) == 2) {

                            $lng = $coordinate[0];
                            $lat = $coordinate[1];

                            if (is_scalar($lng) && is_numeric($lng)) {
                                $lng = (float) $lng;
                            }

                            if (is_scalar($lat) && is_numeric($lat)) {
                                $lat = (float) $lat;
                            }

                            if (is_float($lng) && is_float($lat)) {

                                if ($this->coordinateOrderMode == GeometryInterface::ORDER_LAT_LNG) {

                                    $coordinates[$index] = [
                                        $lat,
                                        $lng,
                                    ];

                                } elseif ($this->coordinateOrderMode == GeometryInterface::ORDER_LNG_LAT) {

                                    $coordinates[$index] = [
                                        $lng,
                                        $lat,
                                    ];

                                }

                                $ok = true;
                            }

                        }

                    }

                }

            }

            if (!$ok) {
                throw new \TypeError("Las coordenadas introducidas no tienen el formato adecuado.");
            }

            $this->coordinates = $coordinates;

        } else {

            return $this->coordinates;

        }

        return $this;
    }

    /**
     * @return array
     */
    public function schema()
    {
        return [
            'type' => self::TYPE,
            'coordinates' => $this->coordinates(),
        ];
    }

    /**
     * @return boolean
     */
    public function hasData()
    {
        return count($this->coordinates) > 0;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->schema();
    }

}
