<?php

/**
 * GeometryInterface.php
 */
namespace PiecesPHP\GeoJson\Geometry;

/**
 * GeometryInterface.
 *
 * Interface que representa una geometría
 *
 * @package     PiecesPHP\GeoJson\Geometry
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 * @see https://geojson.org/schema/Geometry.json
 */
interface GeometryInterface extends \JsonSerializable
{
    /**
     * Ordenar coordenadas en Lat, Lng
     */
    const ORDER_LAT_LNG = 0;
    /**
     * Ordenar coordenadas en Lng, Lat
     */
    const ORDER_LNG_LAT = 1;

    /**
     * @return array
     */
    public function schema();

    /**
     * @return array
     */
    public function coordinates();

}
