<?php

/**
 * Point.php
 */
namespace PiecesPHP\GeoJson\Geometry;

/**
 * Point.
 *
 * Modelo de datos de geometría Point
 *
 * @package     PiecesPHP\GeoJson\Geometry
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 * @see https://geojson.org/schema/Point.json
 */
class Point implements GeometryInterface
{

    const TYPE = 'Point';

    /**
     * @var float
     */
    private $latitude = 0;

    /**
     * @var float
     */
    private $longitude = 0;

    /**
     * @var int
     */
    private $coordinateOrderMode = GeometryInterface::ORDER_LAT_LNG;

    /**
     * @param float $longitude
     * @param float $latitude
     * @param int $coordinateOrderMode
     * @return static
     */
    public function __construct(float $longitude, float $latitude, int $coordinateOrderMode = null)
    {

        if ($coordinateOrderMode !== null && ($coordinateOrderMode == GeometryInterface::ORDER_LAT_LNG || $coordinateOrderMode == GeometryInterface::ORDER_LNG_LAT)) {
            $this->coordinateOrderMode = $coordinateOrderMode;
        }

        $this->longitude = $longitude;
        $this->latitude = $latitude;
    }

    /**
     * @param float $value
     * @return float|static
     */
    public function longitude(float $value = null)
    {

        if (!is_null($value)) {
            $this->longitude = $value;
        } else {
            return $this->longitude;
        }

        return $this;
    }

    /**
     * @param float $value
     * @return float|static
     */
    public function latitude(float $value = null)
    {

        if (!is_null($value)) {
            $this->latitude = $value;
        } else {
            return $this->latitude;
        }

        return $this;
    }

    /**
     * @param float[] $coordinates
     * @return float[]|int[]|static|void
     * @throws \TypeError
     */
    public function coordinates(array $coordinates = null)
    {

        if (is_array($coordinates)) {

            $ok = false;

            if (count($coordinates) == 2) {

                $lng = $coordinates[0];
                $lat = $coordinates[1];

                if (is_scalar($lng) && is_numeric($lng)) {
                    $lng = (float) $lng;
                }

                if (is_scalar($lat) && is_numeric($lat)) {
                    $lat = (float) $lat;
                }

                if (is_float($lng) && is_float($lat)) {

                    $this->longitude($lng);
                    $this->latitude($lat);

                    $ok = true;
                }

            }

            if (!$ok) {
                throw new \TypeError("Las coordenadas introducidas no tienen el formato adecuado.");
            }

            return $this;

        } else {

            if ($this->coordinateOrderMode == GeometryInterface::ORDER_LAT_LNG) {

                return [
                    $this->latitude(),
                    $this->longitude(),
                ];

            } elseif ($this->coordinateOrderMode == GeometryInterface::ORDER_LNG_LAT) {

                return [
                    $this->longitude(),
                    $this->latitude(),
                ];

            }

        }

    }

    /**
     * @return array
     */
    public function schema()
    {
        return [
            'type' => self::TYPE,
            'coordinates' => $this->coordinates(),
        ];
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->schema();
    }

}
