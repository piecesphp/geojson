<?php

/**
 * Feature.php
 */
namespace PiecesPHP\GeoJson;

use PiecesPHP\GeoJson\Geometry\GeometryInterface;
use PiecesPHP\GeoJson\Geometry\LineString;
use PiecesPHP\GeoJson\Geometry\MultiLineString;
use PiecesPHP\GeoJson\Geometry\MultiPolygon;
use PiecesPHP\GeoJson\Geometry\Point;
use PiecesPHP\GeoJson\Geometry\Polygon;

/**
 * Feature.
 *
 * Modelo de datos de una Feature de GeoJson
 *
 * @package     PiecesPHP\GeoJson
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 * @see https://geojson.org/schema/Feature.json
 */
class Feature implements \JsonSerializable
{

    const TYPES_GEOMETRY = [
        Point::TYPE,
        LineString::TYPE,
        Polygon::TYPE,
        MultiLineString::TYPE,
        MultiPolygon::TYPE,
    ];

    /**
     * @var string
     */
    private $typeGeometry = Point::TYPE;

    /**
     * @var array
     */
    private $properties = [];

    /**
     * @var array
     */
    private $coordinates = [];

    /**
     * @var GeometryInterface
     */
    private $geometry = null;

    /**
     * @var int
     */
    private $coordinateOrderMode = GeometryInterface::ORDER_LAT_LNG;

    /**
     * @param array $coordinates
     * @param string $typeGeometry
     * @param array $properties
     * @param int $coordinateOrderMode
     * @return static
     */
    public function __construct(array $coordinates, string $typeGeometry = null, array $properties = [], int $coordinateOrderMode = null)
    {
        if (!in_array($typeGeometry, self::TYPES_GEOMETRY)) {
            throw new \TypeError("El tipo de geometría '{$typeGeometry}' no está soportado.");
        }

        $this->typeGeometry = $typeGeometry !== null ? $typeGeometry : $this->typeGeometry;
        $this->coordinates = $coordinates;
        $this->properties = $properties;

        if ($coordinateOrderMode !== null) {
            $this->coordinateOrderMode = $coordinateOrderMode;
        }

        $this->configGeometry($this->coordinates, $this->coordinateOrderMode);

    }

    /**
     * @param array $coordinates
     * @param int $order
     * @return void
     */
    public function configGeometry(array $coordinates = null, int $order = null)
    {
        $geometry = null;

        $order = $order !== null ? $order : $this->coordinateOrderMode;
        $coordinates = $coordinates !== null ? $coordinates : $this->coordinates;

        if ($this->typeGeometry == Point::TYPE) {

            $geometry = new Point(0, 0, $order);
            $geometry->coordinates($coordinates);

        } elseif ($this->typeGeometry == LineString::TYPE) {

            $geometry = new LineString($coordinates, $order);

        } elseif ($this->typeGeometry == Polygon::TYPE) {

            $geometry = new Polygon($coordinates, $order);

        } elseif ($this->typeGeometry == MultiLineString::TYPE) {

            $geometry = new MultiLineString($coordinates, $order);

        } elseif ($this->typeGeometry == MultiPolygon::TYPE) {

            $geometry = new MultiPolygon($coordinates, $order);

        }

        $this->geometry = $geometry;

    }

    /**
     * @return array
     */
    public function schema()
    {

        return [
            'type' => 'Feature',
            'geometry' => $this->geometry->schema(),
            'properties' => (object) $this->properties,
        ];

    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->schema();
    }

}
