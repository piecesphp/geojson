<?php

/**
 * GeoJson.php
 */
namespace PiecesPHP\GeoJson;

use PiecesPHP\GeoJson\Geometry\GeometryInterface;

/**
 * GeoJson.
 *
 * Modelo de datos de GeoJSON
 *
 * @package     PiecesPHP\GeoJson
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2020
 * @see https://geojson.org/schema/GeoJSON.json
 */
class GeoJson implements \JsonSerializable
{
    /**
     * @var Feature[]
     */
    private $features = [];

    /**
     * @var int
     */
    private $coordinateOrderMode = GeometryInterface::ORDER_LNG_LAT;

    /**
     * @param  Feature[] $features
     * @param  int $coordinateOrderMode
     * @return static
     */
    public function __construct(array $features = [], int $coordinateOrderMode = null)
    {

        if ($coordinateOrderMode !== null) {
            $this->coordinateOrderMode = $coordinateOrderMode;
        }

        $this->features($features);

    }

    /**
     * @param Feature[] $features
     * @return Feature[]|static
     */
    public function features(array $features = null)
    {

        if (is_array($features)) {

            foreach ($features as $feature) {
                $this->addFeature($feature);
            }

        } else {

            return $this->features;

        }

        return $this;

    }

    /**
     * @param Feature $feature
     * @return static
     */
    public function addFeature(Feature $feature)
    {
        $this->features[] = $feature;

        $feature->configGeometry(null, $this->coordinateOrderMode);

        return $this;

    }

    /**
     * @return array
     */
    public function schema()
    {
        $features = [];

        foreach ($this->features as $feature) {

            $features[] = $feature->schema();

        }

        return [
            'type' => 'FeatureCollection',
            'features' => $features,
        ];
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->schema();
    }

}
